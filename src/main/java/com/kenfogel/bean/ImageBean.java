package com.kenfogel.bean;

import java.io.Serializable;

/**
 * Stand in for an entity bean. This just holds the name of an image to display
 *
 * @author Ken Fogel
 */
public class ImageBean implements Serializable {

    private String imageName = "";

    public ImageBean() {
    }

    public ImageBean(String imageName) {
        this.imageName = imageName;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

}
