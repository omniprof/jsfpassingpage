package com.kenfogel.bean;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 * This holds the data you wish to have available on another page
 *
 * @author Ken Fogel
 */
@Named
@SessionScoped
public class CurrentImageName implements Serializable {

    private String ImageName = "No Image";

    public String getImageName() {
        return ImageName;
    }

    public void setImageName(String ImageName) {
        this.ImageName = ImageName;
    }
}
