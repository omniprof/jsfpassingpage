package com.kenfogel.controller;

import com.kenfogel.bean.ImageBean;
import java.io.Serializable;
import java.util.ArrayList;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 * Stand in for a real JPA controller
 *
 * @author Ken Fogel
 */
@Named
@SessionScoped
public class BeanController implements Serializable {

    private final ArrayList<ImageBean> imageBeans;

    /**
     * Create an array list of 4 ImageBeans
     */
    public BeanController() {
        ImageBean ib1 = new ImageBean("image1.JPG");
        ImageBean ib2 = new ImageBean("image2.JPG");
        ImageBean ib3 = new ImageBean("image3.JPG");
        ImageBean ib4 = new ImageBean("image4.JPG");
        imageBeans = new ArrayList<>();
        imageBeans.add(ib1);
        imageBeans.add(ib2);
        imageBeans.add(ib3);
        imageBeans.add(ib4);
    }

    /**
     * This would normally be a call to a JPA controller
     *
     * @return ArrayList of ImageBeans
     */
    public ArrayList<ImageBean> getImageBeans() {
        return imageBeans;
    }
}
