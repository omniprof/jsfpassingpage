package com.kenfogel.actions;

import com.kenfogel.bean.CurrentImageName;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the action that is called when the bean is clicked on
 *
 * @author Ken Fogel
 */
@Named
@SessionScoped
public class ImageAction implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(ImageAction.class);

    @Inject
    private CurrentImageName currentImageName;

    /**
     * This method takes the image name passed in from the index.xhtml page and
     * stores it in a another bean. The original ImageBean is not named and
     * scoped because it is pretending to be an entity. That is why we have the
     * CurrentImageName bean that is named and scoped and so is injected into
     * the ImageAction class and then can be accessed from the show.xhtml page.
     *
     * @param imageName
     * @return Name of next page to go to
     */
    public String doAction(String imageName) {
        LOG.debug(">>>>> doAction: " + imageName);
        currentImageName.setImageName(imageName);
        return "show";
    }
}
